# IDIOM

IDIOM is a collection of performance analysis tools meant for investigating I/O performance problems.
	
## Software

The IDIOM software suite is composed of:

- [EZIOTracer](https://gitlab.com/idiom1/eziotrace), a tool for
  tracing I/O at the application level and the kernel level. EZIOTracer relies on two tracing tools
    
  - [EZTrace](https://gitlab.com/eztrace/eztrace): a tracing tool for
    parallel applications
  
  - [IOTracer](https://github.com/medislam/IOTracer): a tool for
    tracing I/O events in the kernel
  
- [EasyTraceAnalyzer](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer),
  a performance analysis tool suite composed of several tools,
  including:
  
  - [eta_profile](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_profile),
    extracts the profile of an application and detects contention
    problems

  - [eta_mpi_stats](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_mpi_stats)
    provides statistics on the use of MPI functions for a given trace

  - [eta_perf_model](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_perf_model)
    extracts a performance model from a trace, and replays a trace
    with a different performance model
	
  - [eta_merge](https://gitlab.com/parallel-and-distributed-systems/easytraceanalyzer/eta_merge) merges several traces

- [IO-forecast](https://gitlab.com/idiom1/io-forecast) that extract I/O related statistics from a trace
  
  
## Tutorials

### EZTrace documentation
- [Building EZTrace](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/building.md)
- [Using EZTrace](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/using.md)
- [EZTrace plugins](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/plugin.md)
- [Frequently Asked questions](https://gitlab.com/eztrace/eztrace/-/blob/master/doc/faq.md)

### EZTrace tutorials
- [Using EZTrace for MPI applications](https://gitlab.com/eztrace/eztrace-tutorials/-/blob/main/mpi/README.md)
- [Using EZTrace for OpenMP applications](https://gitlab.com/eztrace/eztrace-tutorials/-/blob/main/openmp/README.md)

### EZIOTracer
- [EZIOTracer documentation](https://gitlab.com/idiom1/eziotrace)

### EasyTraceAnalyzer
- [Using eta_mpi_stats](https://parallel-and-distributed-systems.gitlab.io/easytraceanalyzer/eta_web/tutorials/eta_mpi_stats/)
- [Using eta_print](https://parallel-and-distributed-systems.gitlab.io/easytraceanalyzer/eta_web/tutorials/eta_print/)
- [Using eta_profile](https://parallel-and-distributed-systems.gitlab.io/easytraceanalyzer/eta_web/tutorials/eta_profile/)
- [Analyzing an application with ETA](https://parallel-and-distributed-systems.gitlab.io/easytraceanalyzer/eta_web/tutorials/)

## Docker images
- [EZTrace configured for MPICH](https://hub.docker.com/r/eztrace/eztrace.mpich.vite)
- [EZTrace configured for OpenMPI](https://hub.docker.com/r/eztrace/eztrace.openmpi.vite)

- [EZIOTracer](https://hub.docker.com/r/ftrahay/eziotracer)
- [EZIOTracer with an example application](https://hub.docker.com/r/alexvkempen/eziotracer_blender)
